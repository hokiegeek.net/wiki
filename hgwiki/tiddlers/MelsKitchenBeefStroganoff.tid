created: 20181025200601031
modified: 20181025200906090
source: https://www.melskitchencafe.com/the-ultimate-beef-stroganoff/
tags: recipe
title: MelsKitchenBeefStroganoff
type: text/vnd.tiddlywiki

[img[https://www.melskitchencafe.com/wp-content/uploads/beef-stroganoff5-600x900.jpg]]

! Ingredients
* 2-3 pounds stew meat (or a 3-pound beef roast, cubed)
* 1 teaspoon salt
* 1/4 teaspoon black pepper
* 1 medium yellow onion, diced (see note)
* 1/4 teaspoon garlic powder or 1-2 cloves garlic, finely minced
* 1 tablespoon Worcestershire sauce
* 1 1/2 cups beef broth or stock (I use low-sodium)
* 1 tablespoon ketchup
* 1/3 cup flour
* 1/2 cup beef broth, water or apple juice
* 4 to 8 ounces sliced mushrooms (optional)
* 1/2 cup light or regular sour cream

! Directions
# Place the stew meat, salt, pepper and onion in the insert of the Instant Pot. Stir to distribute the seasonings and onion. In a small bowl, combine the garlic powder (or fresh garlic), Worcestershire sauce, beef broth and ketchup. Pour over the meat.
# Secure the lid and valve and cook on high pressure for 35 minutes (press “pressure cook” or “manual” and dial up or down to 35 minutes; the pressure cooker will start on its own). Let the pressure naturally release for 10 minutes and then quick release the remaining pressure. Stir the meat mixture with a wooden spoon to break up any large clumps of meat.
# Combine the flour and broth/water/apple juice in a small bowl, whisking vigorously to combine well (a blender works great as well). The roux should be thick but still pourable. Add additional broth or water, if needed.
# Select the saute function on the Instant Pot. Pour the roux mixture into the Instant Pot, whisking quickly to avoid lumps, and continue mixing until evenly and well combined. Add the mushrooms, if using, and stir to combine.
# Continue cooking on the saute setting, stirring often to prevent sticking, until the stroganoff is thickened. Set the Instant Pot to warm and stir in the sour cream. Serve over pasta, rice or baked potatoes.

! Notes
You can sub in 1 to 2 teaspoons onion powder for the diced onion, if desired.


Freezable Meal: The leftovers of the stroganoff can be frozen. I store the leftovers in a freezer-safe container and then thaw in the refrigerator (usually takes about a day) and reheat over medium-low heat in a saucepan on the stove.