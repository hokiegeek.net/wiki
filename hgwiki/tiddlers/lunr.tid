created: 20181112021227186
modified: 20181115021956541
source: https://www.npmjs.com/package/lunr
tags: npm library programming reference
title: lunr
type: text/vnd.tiddlywiki

! Angular
!! Also required
https://www.npmjs.com/package/@types/lunr

!! Example
```javascript
import * as lunr from 'lunr';

const builder = new lunr.Builder();
builder.ref('id');
builder.field('name');

this.teas.forEach(t => builder.add(t));
builder.add({
    id: 1,
    name: "Foo",
    body: "Foo foo foo!"
});
builder.add({
    id: 2,
    name: "Bar",
    body: "Bar bar bar!"
});

const index = builder.build();
index.search("foo");
```



! ~QueryString
<<<
At its simplest queries can just be a single term, e.g. hello, multiple terms are also supported and will be combined with OR, e.g hello world will match documents that contain either 'hello' or 'world', though those that contain both will rank higher in the results.

Wildcards can be included in terms to match one or more unspecified characters, these wildcards can be inserted anywhere within the term, and more than one wildcard can exist in a single term. Adding wildcards will increase the number of documents that will be found but can also have a negative impact on query performance, especially with wildcards at the beginning of a term.

Terms can be restricted to specific fields, e.g. title:hello, only documents with the term hello in the title field will match this query. Using a field not present in the index will lead to an error being thrown.

Modifiers can also be added to terms, lunr supports edit distance and boost modifiers on terms. A term boost will make documents matching that term score higher, e.g. foo^5. Edit distance is also supported to provide fuzzy matching, e.g. 'hello~2' will match documents with hello with an edit distance of 2. Avoid large values for edit distance to improve query performance.

Each term also supports a presence modifier. By default a term's presence in document is optional, however this can be changed to either required or prohibited. For a term's presence to be required in a document the term should be prefixed with a '+', e.g. +foo bar is a search for documents that must contain 'foo' and optionally contain 'bar'. Conversely a leading '-' sets the terms presence to prohibited, i.e. it must not appear in a document, e.g. -foo bar is a search for documents that do not contain 'foo' but may contain 'bar'.

To escape special characters the backslash character '\' can be used, this allows searches to include characters that would normally be considered modifiers, e.g. foo\~2 will search for a term "foo~2" instead of attempting to apply a boost of 2 to the search term "foo".
<<< https://lunrjs.com/docs/lunr.Index.html#~QueryString