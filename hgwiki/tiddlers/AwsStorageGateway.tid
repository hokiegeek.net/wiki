course-name: aws
created: 20180314235228000
list: 
modified: 20181014022736998
tags: aws S3
title: AwsStorageGateway
type: text/vnd.tiddlywiki

* A service that connects an on-premises software appliance with could-based storage to provide seamless and secure integration between an orgs’ IT env and AWS’ storage infra. It enables you to securely store data to the AWS cloud for scalable and cost-effective storage.
* The appliance is downloaded as a VM installed as a host in your datacenter.
* Four types of storage gateways
** File Gateway (NFS) {brand new}
*** Stored as S3 buckets and accessed through NFS mount point. Ownership, perms, and timestamps are durably stored in S3 in the user-metadata of the object associated with the file. Once objects are transferred to S3, they can be managed and native S3 objects.
** Volumes Gateway (iSCSI)
*** Volume interface presents your applications with disk volumes using the iSCSI block protocol. Data written to these volumes can be asynchronously backed up as point-in-time snapshots of your volumes, and stored in the cloud as Amazon EBS snapshots. Snapshots are incremental backups that capture only changed blocks and are then compressed.
*** Stored Volumes (store everything on-prem)
**** Asynchronously backs up all of your local data to AWS. Low-latency access to your on-prem apps with durable, off-site backups.
**** 1 GB - 16 TB in size
**** {{image:awsStoredVolsDiagram}}
*** Cached Volumes (only latest stored locally)
**** Lets you use S3 as your primary data storage while retaining frequently accessed data locally in your storage gateway. They minimize the need to scale your on-prem storage infra while still providing your apps with low-latency access to data.
**** 1 GB - 32 TB in size
**** {{image:awsCachedVolsDiagram}}
** Tape Gateway (VTL)
*** Virtual Tape Library interface which allows you to use your tape-based backup apps to store on virtual tape cartridges created on the tape gateway. Each tape gateway is preconfigured with a media changer and tape drives which are available to your your existing client backup apps as iSCSI devices.
*** {{image:awsVTLDiagram}}
