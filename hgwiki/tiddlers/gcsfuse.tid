created: 20181002100120000
modified: 20181011023654147
source: https://cloud.google.com/storage/docs/gcs-fuse
tags: gcp reference
title: gcsfuse
type: text/vnd.tiddlywiki

@@background-color: #ff7f7f;
@@border: 1px solid #990000;
@@padding: 5px;
''WARNING:'' This is seriously slow.
<br />
As a result, the VCS logic in the prompt kicks its butt and makes the terminal practically
unusable.
<br />
To deal with this, I added this env var which turns off the VCS logic `PROMPT_NO_REPO_INFO`.
<br />
Export it before navigating to a git repo that has a gcsfuse mount.
@@

! Basic use
!! Mounting
```bash
mkdir /path/to/mount
gcsfuse --implicit-dirs <bucket-name> /path/to/mount
```

Without `implicit-dirs` it doesn't bring down the pseudo-directory objects.

!! Unmounting
```bash
umount /path/to/mount
```

or

```bash
fusermount -u /path/to/mount
```

! Installing
!! Arch
Needed packages are `fuse` and `gcsfuse`

And will either need to `modprobe fuse` or reboot if the kernel was recently updated.

!! Ubuntu / Debian
```bash
echo "deb http://packages.cloud.google.com/apt gcsfuse-$(lsb_release -c -s) main" | sudo tee /etc/apt/sources.list.d/gcsfuse.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y gcsfuse
```

!! OSX
```bash
brew cask install osxfuse
brew install gcsfuse
```

! Links
* https://github.com/GoogleCloudPlatform/gcsfuse/blob/master/docs/installing.md
* https://github.com/GoogleCloudPlatform/gcsfuse/blob/master/docs/semantics.md#implicit-directories
