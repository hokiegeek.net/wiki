# AI

# Client

# Tsumego

* [BW-Go](https://play.google.com/store/apps/details?id=mco.prj.app.bwgo)
 * [BW-DGS](https://play.google.com/store/apps/details?id=mco.prj.srv.bwdgs) 
 * [BW-Joseki](https://play.google.com/store/apps/details?id=mco.prj.app.bwkjd) 
 * [BWGnuGo](https://play.google.com/store/apps/details?id=mco.prj.srv.bwgnugo) 
* [Champion Go](https://play.google.com/store/apps/details?id=jp.co.unbalance.android.igoen) 
* [CrazyStone](https://play.google.com/store/apps/details?id=jp.co.unbalance.android.gocsdllite) 
* [ElyGo Lite](https://play.google.com/store/apps/details?id=lrstudios.games.ego.lite) 
* [GNU Go](https://play.google.com/store/apps/details?id=lrstudios.games.gnugo) 
* [Go](https://play.google.com/store/apps/details?id=uk.co.aifactory.go) 
* [Go Dojo](https://play.google.com/store/apps/details?id=pl.happydroid.goess) 
* [Go Joseki](https://play.google.com/store/apps/details?id=info.goro3goro.igojoseki) 
* [Go Tesuji](https://play.google.com/store/apps/details?id=info.goro3goro.igotesuji) 
* [Gobandroid](https://play.google.com/store/apps/details?id=org.ligi.gobandroid_hd) 
* [gobandroid ai gnugo](https://play.google.com/store/apps/details?id=org.ligi.gobandroidhd.ai.gnugo) 
* [GoGrinder](https://play.google.com/store/apps/details?id=com.gogrinder) 
* [GridMaster Free](https://play.google.com/store/apps/details?id=nl.tengen.gridmaster) 
* [Guess the Move](https://play.google.com/store/apps/details?id=thego.gtm.guessthemove)  
* [Hactar Go](https://play.google.com/store/apps/details?id=net.gowrite) 
* [Joseki](https://play.google.com/store/apps/details?id=nl.ingele.gosujoseki) 
* [KGS](https://play.google.com/store/apps/details?id=com.gokgs.client.android) 
* [Master of Go](https://play.google.com/store/apps/details?id=com.maryharry.masterofgo) 
* [Online Go](https://play.google.com/store/apps/details?id=io.zenandroid.onlinego) 
* [Pandanet(Go)](https://play.google.com/store/apps/details?id=be.gentgo.tetsuki) 
* [Tsumego](https://play.google.com/store/apps/details?id=com.zhengping.weiqideathlife) 
* [Tsumego Pro](https://play.google.com/store/apps/details?id=net.lrstudios.android.tsumego_workshop) 